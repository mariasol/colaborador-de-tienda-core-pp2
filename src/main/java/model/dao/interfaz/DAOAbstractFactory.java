package model.dao.interfaz;

public interface DAOAbstractFactory {
	
	public Employee_DAO_Interfaz createEmployeeDAO();
	
	public Notification_DAO_Interfaz createNotificationDAO();
	
	public Price_DAO_Interfaz createPriceDAO();
	
	public Product_DAO_Interfaz createProductDAO();
	
	public Purchase_DAO_Interfaz createPurchaseDAO();
	
	public RecomendedStock_DAO_Interfaz createRecomendedStockDAO();
	
	public ShoppingHistory_DAO_Interfaz createShoppingHistoryDAO();
	
	public Stock_DAO_Interfaz createStockDAO();
	
}
