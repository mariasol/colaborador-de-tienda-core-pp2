package model.dao.interfaz;

import java.util.List;

import model.dto.Employee_DTO;

public interface Employee_DAO_Interfaz {
	
	public boolean insert(Employee_DTO newEmployee);
	
	public boolean delete(Employee_DTO employee);
	
	public boolean update(Employee_DTO employee);
	
	public List<Employee_DTO> readAll();

}
