package model.dao.interfaz;

import java.util.List;

import model.dto.Price_DTO;

public interface Price_DAO_Interfaz {

	public boolean insert(Price_DTO newPrice);
	
	public boolean delete(Price_DTO price);
	
	public boolean update(Price_DTO price);
	
	public List<Price_DTO> readAll();
	
}
