package model.dao.interfaz;

import java.util.List;

import model.dto.ShoppingHistory_DTO;

public interface ShoppingHistory_DAO_Interfaz {

	public boolean insert(ShoppingHistory_DTO newShoppingHistory);
	
	public boolean delete(ShoppingHistory_DTO shoppingHistory);
	
	public boolean update(ShoppingHistory_DTO shoppingHistory);
	
	public List<ShoppingHistory_DTO> readAll();
	
}
