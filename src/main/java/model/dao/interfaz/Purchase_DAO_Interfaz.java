package model.dao.interfaz;

import java.util.List;

import model.dto.Purchase_DTO;

public interface Purchase_DAO_Interfaz {

	public boolean insert(Purchase_DTO newPurchase);
	
	public boolean delete(Purchase_DTO purchase);
	
	public boolean update(Purchase_DTO purchase);
	
	public List<Purchase_DTO> readAll();
	
}
