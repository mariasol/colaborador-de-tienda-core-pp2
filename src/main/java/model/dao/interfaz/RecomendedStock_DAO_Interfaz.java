package model.dao.interfaz;

import java.util.List;

import model.dto.RecomendedStock_DTO;

public interface RecomendedStock_DAO_Interfaz {

	public boolean insert(RecomendedStock_DTO newRecomendedStock);
	
	public boolean delete(RecomendedStock_DTO recomendedStock);
	
	public boolean update(RecomendedStock_DTO recomendedStock);
	
	public List<RecomendedStock_DTO> readAll();
	
}
