package model.dao.interfaz;

import java.util.List;

import model.dto.Product_DTO;

public interface Product_DAO_Interfaz {

	public boolean insert(Product_DTO newProduct);
	
	public boolean delete(Product_DTO product);
	
	public boolean update(Product_DTO product);
	
	public Product_DTO getById(int id);
	
	public List<Product_DTO> readAll();
	
}
