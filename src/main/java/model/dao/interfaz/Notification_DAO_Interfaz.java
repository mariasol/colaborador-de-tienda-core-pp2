package model.dao.interfaz;

import java.util.List;

import model.dto.Notification_DTO;

public interface Notification_DAO_Interfaz {

	public boolean insert(Notification_DTO newNotification);
	
	public boolean delete(Notification_DTO notification);
	
	public boolean update(Notification_DTO notification);
	
	public List<Notification_DTO> readAll();
	
}
