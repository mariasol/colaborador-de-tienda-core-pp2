package model.dao.interfaz;

import java.util.List;

import model.dto.Stock_DTO;

public interface Stock_DAO_Interfaz {

	public boolean insert(Stock_DTO newStock);
	
	public boolean delete(Stock_DTO stock);
	
	public boolean update(Stock_DTO stock);
	
	public List<Stock_DTO> readAll();
	
}
