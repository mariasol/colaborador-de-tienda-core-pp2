package model.dao.implementation;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import model.dao.interfaz.Product_DAO_Interfaz;
import model.dto.Product_DTO;

public class Product_JSON implements Product_DAO_Interfaz {

	private String pathFile;//Ver comentario abajo
	private JSONArray datos; //TODO: ver si es correcto llamarlo desde aca | lo instancio ac� porque se desaconseja usar Singleton
	
	public Product_JSON(String pathFile) {
		super();
		this.pathFile = pathFile;
		this.datos = util.Json.leerJson(this.pathFile);
	}

	@Override
	public boolean insert(Product_DTO newProduct) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean delete(Product_DTO product) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean update(Product_DTO product) {
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public Product_DTO getById(int id) {
		List<Product_DTO> productsArrayList = this.readAll();
		Product_DTO product = new Product_DTO();
		for(int i=0;i<productsArrayList.size();i++) {
			if(productsArrayList.get(i).getId()==String.valueOf(id)) {
				product.setId(productsArrayList.get(i).getId());
				product.setImageUrl(productsArrayList.get(i).getImageUrl());
				product.setName(productsArrayList.get(i).getName());
				break;
			}
		}
		return product;
	}

	@Override
	public List<Product_DTO> readAll() {
		List<Product_DTO> productsArrayList = new ArrayList<Product_DTO>();
		
        JSONObject datos_prod=(JSONObject)datos.get(1); //me paro en el objeto 1 (donde guardo a los productos)
        JSONArray productsList=(JSONArray)datos_prod.get("products"); //creo un array de empleados
        JSONObject products=(JSONObject)productsList.get(0);
        for (int i=0;i<products.size();i++) {
        	JSONObject productJSON=(JSONObject)products.get(String.valueOf(i));
        	
        	Product_DTO producto=new Product_DTO();
        	 
        	producto.setId(String.valueOf(i));
        	producto.setName((String)productJSON.get("name"));
        	producto.setImageUrl((String) productJSON.get("imgUrl"));
        	
        	productsArrayList.add(producto);
        }
        
		return productsArrayList;
	}

	

}
