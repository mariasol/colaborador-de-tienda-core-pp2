package model.dao.implementation;

import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import model.dao.interfaz.Employee_DAO_Interfaz;
import model.dto.Employee_DTO;

public class Employee_JSON implements Employee_DAO_Interfaz {
	
	
	private String pathFile;//Ver comentario abajo
	private JSONArray datos; //TODO: ver si es correcto llamarlo desde aca | lo instancio ac� porque se desaconseja usar Singleton
	
	public Employee_JSON(String pathFile) {
		super();
		this.pathFile = pathFile;
		this.datos = util.Json.leerJson(this.pathFile);
	}

	
	@Override
	public boolean insert(Employee_DTO newEmployee) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean delete(Employee_DTO employee) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean update(Employee_DTO employee) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List<Employee_DTO> readAll() {
		List<Employee_DTO> employeesList = new ArrayList<Employee_DTO>();
		
        JSONObject datos_emp=(JSONObject)datos.get(0); //me paro en el objeto 0 (donde guardo a los empleados)
        JSONArray empleadosList=(JSONArray)datos_emp.get("employees"); //creo un array de empleados
        JSONObject empleados=(JSONObject)empleadosList.get(0);
        for (int i=0;i<empleados.size();i++) {
        	JSONObject empleadoJSON=(JSONObject)empleados.get(String.valueOf(i));
        	
        	Employee_DTO empleado=new Employee_DTO();
        	empleado.setName(empleadoJSON.get("firstName")+" "+empleadoJSON.get("lastName"));
        	empleado.setDni((String)empleadoJSON.get("dni"));
        	empleado.setEmail((String)empleadoJSON.get("email"));
        	empleado.setMailAlert(Boolean.valueOf((String)empleadoJSON.get("mailAlert")));
        	 
        	employeesList.add(empleado);
        }
        
		return employeesList;
	}

}
