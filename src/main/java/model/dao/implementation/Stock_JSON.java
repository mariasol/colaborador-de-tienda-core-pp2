package model.dao.implementation;

import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import model.dao.interfaz.Stock_DAO_Interfaz;
import model.dto.Stock_DTO;

public class Stock_JSON implements Stock_DAO_Interfaz {

	private String pathFile;//Ver comentario abajo
	private JSONArray datos; //TODO: ver si es correcto llamarlo desde aca | lo instancio ac� porque se desaconseja usar Singleton
	private Product_JSON productJSON;
	public Stock_JSON(String pathFile) {
		super();
		this.pathFile = pathFile;
		this.datos = util.Json.leerJson(this.pathFile);
		productJSON=new Product_JSON(pathFile);
	}
	
	@Override
	public boolean insert(Stock_DTO newStock) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean delete(Stock_DTO stock) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean update(Stock_DTO stock) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List<Stock_DTO> readAll() {
		List<Stock_DTO> stockOfProductsArrayList = new ArrayList<Stock_DTO>();
		
        JSONObject datos_stock=(JSONObject)datos.get(1); //me paro en el objeto 1 (donde guardo a los productos)
        JSONArray stockProductsList=(JSONArray)datos_stock.get("products"); //creo un array de empleados
        JSONObject stocks=(JSONObject)stockProductsList.get(0);
        for (int i=0;i<stocks.size();i++) {
        	JSONObject stockOfProductJSON=(JSONObject)stocks.get(String.valueOf(i));
        	
        	Stock_DTO stock=new Stock_DTO();
        	stock.setId(String.valueOf(i));
        	stock.setQuantity((Integer)stockOfProductJSON.get("quantity"));
        	stock.setProduct(productJSON.getById((Integer)stockOfProductJSON.get("prodId")));
        	stockOfProductsArrayList.add(stock);
        }
        
		return stockOfProductsArrayList;
	}

}
