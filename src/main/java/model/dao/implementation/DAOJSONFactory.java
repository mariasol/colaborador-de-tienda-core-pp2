package model.dao.implementation;

import model.dao.interfaz.DAOAbstractFactory;
import model.dao.interfaz.Employee_DAO_Interfaz;
import model.dao.interfaz.Notification_DAO_Interfaz;
import model.dao.interfaz.Price_DAO_Interfaz;
import model.dao.interfaz.Product_DAO_Interfaz;
import model.dao.interfaz.Purchase_DAO_Interfaz;
import model.dao.interfaz.RecomendedStock_DAO_Interfaz;
import model.dao.interfaz.ShoppingHistory_DAO_Interfaz;
import model.dao.interfaz.Stock_DAO_Interfaz;

public class DAOJSONFactory implements DAOAbstractFactory{

	private String jsonPathFile;
	
	public DAOJSONFactory(String jsonPathFile) {
		super();
		this.jsonPathFile = jsonPathFile;
	}	
	
	@Override
	public Employee_DAO_Interfaz createEmployeeDAO() {
		// TODO Auto-generated method stub
		return new Employee_JSON(this.jsonPathFile);
	}

	@Override
	public Notification_DAO_Interfaz createNotificationDAO() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Price_DAO_Interfaz createPriceDAO() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Product_DAO_Interfaz createProductDAO() {
		// TODO Auto-generated method stub
		return new Product_JSON(this.jsonPathFile);
	}

	@Override
	public Purchase_DAO_Interfaz createPurchaseDAO() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RecomendedStock_DAO_Interfaz createRecomendedStockDAO() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ShoppingHistory_DAO_Interfaz createShoppingHistoryDAO() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Stock_DAO_Interfaz createStockDAO() {
		// TODO Auto-generated method stub
		return null;
	}
	
	

}
