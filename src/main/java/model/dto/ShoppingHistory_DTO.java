package model.dto;

import java.util.List;

public class ShoppingHistory_DTO {
	private List<Purchase_DTO> shoppingHistory;
	private String id;
	
	public List<Purchase_DTO> getShoppingHistory() {
		return shoppingHistory;
	}
	public void setShoppingHistory(List<Purchase_DTO> shoppingHistory) {
		this.shoppingHistory = shoppingHistory;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
}