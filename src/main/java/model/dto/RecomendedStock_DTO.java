package model.dto;

public class RecomendedStock_DTO {
	private Product_DTO product;
	private int quantity;
	private String id;
	
	public Product_DTO getProduct() {
		return product;
	}
	public void setProduct(Product_DTO product) {
		this.product = product;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
}