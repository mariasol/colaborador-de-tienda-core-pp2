package model.dto;

import java.util.Date;

public class Purchase_DTO {
	private Product_DTO product;
	private Price_DTO price;
	private Date date;
	private String id;
	
	public Product_DTO getProduct() {
		return product;
	}
	public void setProduct(Product_DTO product) {
		this.product = product;
	}
	public Price_DTO getPrice() {
		return price;
	}
	public void setPrice(Price_DTO price) {
		this.price = price;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
}
