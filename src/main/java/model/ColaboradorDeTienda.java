package model;

import java.util.List;

import model.dao.interfaz.DAOAbstractFactory;
import model.dao.interfaz.Employee_DAO_Interfaz;
import model.dao.interfaz.Notification_DAO_Interfaz;
import model.dao.interfaz.Price_DAO_Interfaz;
import model.dao.interfaz.Product_DAO_Interfaz;
import model.dao.interfaz.Purchase_DAO_Interfaz;
import model.dao.interfaz.RecomendedStock_DAO_Interfaz;
import model.dao.interfaz.ShoppingHistory_DAO_Interfaz;
import model.dao.interfaz.Stock_DAO_Interfaz;
import model.dto.Employee_DTO;
import model.dto.Notification_DTO;
import model.dto.Price_DTO;
import model.dto.Product_DTO;
import model.dto.Purchase_DTO;
import model.dto.RecomendedStock_DTO;
import model.dto.ShoppingHistory_DTO;
import model.dto.Stock_DTO;

public class ColaboradorDeTienda {
	
	private Employee_DAO_Interfaz employee;
	private Notification_DAO_Interfaz notification;
	private Price_DAO_Interfaz price;
	private Product_DAO_Interfaz product;
	private Purchase_DAO_Interfaz purchase;
	private RecomendedStock_DAO_Interfaz recomendedStock;
	private ShoppingHistory_DAO_Interfaz shoppingHistory;
	private Stock_DAO_Interfaz stock;
	
	
	public ColaboradorDeTienda(DAOAbstractFactory persistenceMethod) {
		this.employee = persistenceMethod.createEmployeeDAO();
		this.notification = persistenceMethod.createNotificationDAO();
		this.price = persistenceMethod.createPriceDAO();
		this.product = persistenceMethod.createProductDAO();
		this.purchase = persistenceMethod.createPurchaseDAO();
		this.recomendedStock = persistenceMethod.createRecomendedStockDAO();
		this.shoppingHistory = persistenceMethod.createShoppingHistoryDAO();
		this.stock = persistenceMethod.createStockDAO();
	}

	/*
	 *   --------------- Employee --------------------------------
	 */
	
	public void addEmployee(Employee_DTO newEmployee) {
		this.employee.insert(newEmployee);
	}
	
	public void deleteEmployee(Employee_DTO employee) {
		this.employee.delete(employee);
	}
	
	public void updateEmployee(Employee_DTO employee) {
		this.employee.update(employee);
	}
	
	public List<Employee_DTO> readAllEmployees() {
		return this.employee.readAll();
	}
	
	/*
	 *   --------------- Notification --------------------------------
	 */
	
	public void addNotification(Notification_DTO newNotification) {
		this.notification.insert(newNotification);
	}
	
	public void deleteNotification(Notification_DTO notification) {
		this.notification.delete(notification);
	}
	
	public void updateNotification(Notification_DTO notification) {
		this.notification.update(notification);
	}
	
	public List<Notification_DTO> readAllNotifications() {
		return this.notification.readAll();
	}
	
	/*
	 *   --------------- Price --------------------------------
	 */
	
	public void addPrice(Price_DTO newPrice) {
		this.price.insert(newPrice);
	}
	
	public void deletePrice(Price_DTO price) {
		this.price.delete(price);
	}
	
	public void updatePrice(Price_DTO price) {
		this.price.update(price);
	}
	
	public List<Price_DTO> readAllPrices() {
		return this.price.readAll();
	}
	
	/*
	 *   --------------- Product --------------------------------
	 */
	
	public void addProduct(Product_DTO newProduct) {
		this.product.insert(newProduct);
	}
	
	public void deleteProduct(Product_DTO product) {
		this.product.delete(product);
	}
	
	public void updateProduct(Product_DTO product) {
		this.product.update(product);
	}
	
	public List<Product_DTO> readAllProducts() {
		return this.product.readAll();
	}
	
	/*
	 *   --------------- Purchase --------------------------------
	 */
	
	public void addPurchase(Purchase_DTO newPurchase) {
		this.purchase.insert(newPurchase);
	}
	
	public void deletePurchase(Purchase_DTO purchase) {
		this.purchase.delete(purchase);
	}
	
	public void updatePurchase(Purchase_DTO purchase) {
		this.purchase.update(purchase);
	}
	
	public List<Purchase_DTO> readAllPurchases() {
		return this.purchase.readAll();
	}
	
	/*
	 *   --------------- Recomended Stock --------------------------------
	 */
	
	public void addRecomendedStock(RecomendedStock_DTO newRecomendedStock) {
		this.recomendedStock.insert(newRecomendedStock);
	}
	
	public void deleteRecomendedStock(RecomendedStock_DTO recomendedStock) {
		this.recomendedStock.delete(recomendedStock);
	}
	
	public void updateRecomendedStock(RecomendedStock_DTO recomendedStock) {
		this.recomendedStock.update(recomendedStock);
	}
	
	public List<RecomendedStock_DTO> readAllRecomendedStocks() {
		return this.recomendedStock.readAll();
	}
	
	/*
	 *   --------------- Shopping History --------------------------------
	 */
	
	public void addShoppingHistory(ShoppingHistory_DTO newShoppingHistory) {
		this.shoppingHistory.insert(newShoppingHistory);
	}
	
	public void deleteShoppingHistory(ShoppingHistory_DTO shoppingHistory) {
		this.shoppingHistory.delete(shoppingHistory);
	}
	
	public void updateShoppingHistory(ShoppingHistory_DTO shoppingHistory) {
		this.shoppingHistory.update(shoppingHistory);
	}
	
	public List<ShoppingHistory_DTO> readAllShoppingHistories() {
		return this.shoppingHistory.readAll();
	}
	
	/*
	 *   --------------- Stock --------------------------------
	 */
	
	public void addStock(Stock_DTO newStock) {
		this.stock.insert(newStock);
	}
	
	public void deleteStock(Stock_DTO stock) {
		this.stock.delete(stock);
	}
	
	public void updateStock(Stock_DTO stock) {
		this.stock.update(stock);
	}
	
	public List<Stock_DTO> readAllStocks() {
		return this.stock.readAll();
	}
	
	
}
