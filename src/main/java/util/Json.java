package util;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class Json {

	public static JSONArray leerJson(String pathFile) {
		JSONParser jsonParser = new JSONParser();
		JSONArray archivoJSON = null;
		try 
        {
            //Read JSON file
        	FileReader reader = new FileReader(pathFile);
            Object obj = jsonParser.parse(reader);
 
            archivoJSON = (JSONArray) obj;
            //System.out.println(employeeList);
             
            
 
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return archivoJSON;
	}

}
